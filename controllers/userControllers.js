const User = require("../models/Users");

const Course = require("../models/Course");

const bcrypt = require('bcrypt');

const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		if(result.length > 0){
			return true;
		} else {

			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		//10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})

}


module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		if(result == null){
			return false;
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)


			if(isPasswordCorrect){
			
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}

		};
	});
};


//Retrieve user details

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
		result.password = "";

		return result;
	})
}


//Enroll user to a course

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then( user => {

		user.enrollments.push({ courseId: data.courseId});

		//save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			}else {
				return true
			}
		})
	});


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error) {
				return false;
			}else {
				return true;
			}
		})
	});


	if(isUserUpdated && isCourseUpdated){
		return true;
	}else {
		return false;
	}

}


module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
		result.password = "";

		return result;
	})
}







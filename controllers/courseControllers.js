const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})

}

//Answer
// module.exports.addCourse = (reqBody) => {

// 	console.log(reqBody);

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((course, error) => {
// 		if (error) {
// 			return false;
// 		} else{
// 			return true;
// 		}
// 	})
// }




//Retrieving All courses

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}


//Retrieve all ACTIVE courses

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}

//Retrieve SPECIFIC course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}


//Update a course

module.exports.updateCourse = (courseId, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		}else {
			return true;
		}
	})

}



//Archive a course

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	}

	return Course.findByIdAndUpdate(reqParams, updateActiveField).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}














